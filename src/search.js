import React from 'react';
import './App.js';
import './search.css';
import * as FontAwesome from 'react-icons/lib/fa';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
// import PageCadastro from './PageCadastro.js';

class SearchBar extends React.Component {
  render() {
    return (
      <div className="search-bar">
        <FontAwesome.FaSearch className="search-bar-icon-search" />

        <input className="input-search-bar" placeholder="Pesquisar Contatos" />
        <Link to="/pageCadastro">
          <FontAwesome.FaUserPlus className="search-bar-icon-user" />
        </Link>
      </div>
    );
  }
}

export default SearchBar;
