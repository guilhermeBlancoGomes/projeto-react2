import React, { Component } from 'react';
import './App.css';
import SearchBar from './search';
import ListContact from './listContact';
// import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
class App extends Component {
  render() {
    return (
      <div className="App">
        <SearchBar />
        <p />
        <ListContact />
      </div>
    );
  }
}

export default App;
