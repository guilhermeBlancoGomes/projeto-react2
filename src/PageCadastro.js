import React from 'react';
import './pageCadastro.css';

class PageCadastro extends React.Component {
  render() {
    return (
      <div className="container-principal">
        <div className="container-principal--cadastro">
          <div className="input-principal-cadastro-imagem" />
          <div className="input-principal-cadastro-nome">
            <input className="input-form" placeholder="Nome completo" />
          </div>
          <div className="input-principal-cadastro-email">
            <input className="input-form" placeholder="E-mail" />
          </div>
          <div className="btn-principal-cadastro">
            <button>Cadastrar</button>
          </div>
        </div>
      </div>
    );
  }
}

export default PageCadastro;
