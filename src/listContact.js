import React, { Component } from 'react';
import './listContact.css';
import * as FontAwesome from 'react-icons/lib/fa';

class ListContact extends Component {
  render() {
    const list = [
      {
        nome: 'Guilherme Blanco Gomes',
        email: 'guilherme.gomes@nextel.com.br'
      },
      {
        nome: 'teste da silva',
        email: 'teste@nextel.com.br'
      },
      {
        nome: 'teste da silva',
        email: 'teste@nextel.com.br'
      },
      {
        nome: 'teste da silva',
        email: 'teste@nextel.com.br'
      }
    ];
    return list.map(listContact => (
      <div key={Math.random() * 2} className="ctn-list-people">
        <div className="list-people-img">
          {/* div da imagem */}
          <img src="./foto1.jpg" alt="Imagem usuário" />
        </div>
        <div className="list-people-dados">
          {/* div dos dados da pessoa */}
          <strong>
            <p>{listContact.nome}</p>
          </strong>
          <p>
            <span className="list-people-dados-span">{listContact.email}</span>
          </p>
        </div>
        <div className="list-people-btn-excluir">
          <a href="">
            <FontAwesome.FaClose />
          </a>
        </div>
      </div>
    ));
  }
}

export default ListContact;
